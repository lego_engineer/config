# ~/.bashrc

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# Attach tmux if an unnamed session exists, else create a new one
if command -v tmux > /dev/null 2>&1 && [ -z ${TMUX+x} ]; then
    TMUX_SESS=$(tmux ls | grep -v '(attached)' |\
                awk -F":" '{ print $1 }' | grep '^[0-9]\+$' |\
                head -n 1
               )
    [ ! -z $TMUX_SESS ] && exec tmux -2 attach -t ${TMUX_SESS} || exec tmux -2
fi

# Set default umask
umask 022

# History file settings
shopt -s histappend #Append, don't overwrite
HISTCONTROL=ignoreboth #Ignore duplicates or lines starting with space
HISTSIZE= #Unlimited
HISTFILESIZE= #Unlimited
export PROMPT_COMMAND="history -n; history -w; history -c; history -r"

# Globbing options
shopt -s globstar # ** will match subdirectories

# Window Settings
shopt -s checkwinsize #Update the window ($LINES, $COLUMNS) after each command

## Prompt Settings
# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

case "$TERM" in
    xterm-color|*-256color) color_prompt=yes;; # If using color prompt, assume 256 colors
esac

if [ "$color_prompt" = yes ]; then
    # Color variables
    goldenrod="\[$(tput setaf 220)\]"
    forestgreen="\[$(tput setaf 22)\]"
    lavender="\[$(tput setaf 98)\]"
    reset="\[$(tput sgr0)\]"

    # Color part
    PS1="${goldenrod}\u${reset}@${forestgreen}\h${reset}:${lavender}\W\$${reset}"
    # Chroot part
    PS1="${reset}"'${debian_chroot:+($debian_chroot)}'"$PS1"
    unset goldenrod forestgreen lavender reset
else
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\W\$ '
fi
unset color_prompt

# Colored Settings
# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    alias grep='grep --color=auto'
fi

# colored GCC warnings and errors
export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'


export LD_LIBRARY_PATH=/usr/local/lib:$LD_LIBRARY_PATH
[ -f ~/.pystartup ] && PYSTARTUP="$HOME/.pystartup"

# Source aliases
[ -f ~/.bash_aliases ] && . ~/.bash_aliases

# Source bash completion script
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

# Try sourcing llvm
source scl_source enable llvm-toolset-7 2>/dev/null >/dev/null

## Path Settings
PATH=${HOME}/.local/bin:${PATH}
PATH=${HOME}/bin:${PATH}
PATH=${PATH}:/usr/local/bin
