alias ls='ls --color=auto'
alias ll='ls -la'
alias cd..='cd ..'
alias gits='git status'
alias gitb='git branch'
alias gitl='git log'
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'
alias j='jobs -l'
alias h='history'
alias hs='history | grep'
alias find='find -O3'
alias findh='find -O3 . -iname'
alias psmem='ps auxf | sort -nr -k 4'
alias pscpu='ps auxf | sort -nr -k 3'
alias psmem10='ps auxf | sort -nr -k 4 | head -10'
alias pscpu10='ps auxf | sort -nr -k 3 | head -10'
alias meminfo='free -m -l -t'
alias cpuinfo='lscpu'
alias wget='wget -c'
alias g++='g++ -Wall -Wextra -Werror \
          -Winit-self -Wshadow -Wpointer-arith -Wcast-qual \
          -Wmissing-declarations \
          -pipe -std=c++11'
alias maken='make -j`nproc`'
