" Plugin Manager --------------------------------------------------------------
" How to install vim plug
" curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
" In vim, run :PlugInstall
if !empty(glob("~/.vim/autoload/plug.vim"))
    call plug#begin('~/.vim/plugged')
    Plug 'Valloric/YouCompleteMe', { 'do': './install.py' }
    Plug 'ntpeters/vim-better-whitespace'
    Plug 'tpope/vim-fugitive'
    Plug 'tpope/vim-surround'
    Plug 'scrooloose/nerdtree'
    Plug 'godlygeek/tabular'
    Plug 'nathanaelkane/vim-indent-guides'
    Plug 'flazz/vim-colorschemes'
    Plug 'ervandew/supertab'
    Plug 'w0rp/ale'
    Plug 'godlygeek/csapprox'
    Plug 'ryanoasis/vim-devicons'
    Plug 'tpope/vim-markdown'
    call plug#end()
endif

" Plugin Flags ----------------------------------------------------------------
"" YouCompelteMe
let g:ycm_autoclose_preview_window_after_completion = 1

"" vim-better-whitespace
let g:strip_whitelines_at_eof=1
let g:strip_whitespace_on_save=1

"" Ale
let g:ale_echo_msg_format = '%linter% says: %code: %%s'
let g:ale_lint_on_enter = 1
let g:ale_linters = {'cpp' : ['cc']}

" General Setting -------------------------------------------------------------
" Set a nicer colorscheme
try
    colorscheme molokai
catch
    colorscheme slate
endtry

" Enable 256 colors palette in Gnome Terminal (because it is kinda broken)
if $COLORTERM == 'gnome-terminal'
    set t_Co=256
endif

" Enable the wildmenu (wildcard commandline options)
set wildmenu

" Ignore compiled files in wildmenu
set wildignore=*.o,*~,*.pyc
if has("win16") || has("win32")
    set wildignore+=.git\*,.hg\*,.svn\*
else
    set wildignore+=*/.git/*,*/.hg/*,*/.svn/*,*/.DS_Store
endif

" Enable filetype based colorschemes, plugins, and indentation.
filetype indent plugin on

" Disable error bell
set visualbell t_vb=

" Enable syntax highlighting
syntax on

" Set utf8 as the standard
set encoding=utf8

" Set the file format (priority: unix, windows, mac)
set ffs=unix,dos,mac

" Ignore case when searching, unless the pattern includes uppercase
set ignorecase smartcase

" Highlight search results
set hlsearch

" Start finding items as soon as you start searching.
set incsearch

" Set up smart tab (tabs differently depending on current line position)
set smarttab

" Set auto indent and smart indent
set autoindent smartindent

" Re-source the vim config after it is written
autocmd! bufwritepost .vimrc source $MYVIMRC

" Makes paste work
set nocompatible
set showcmd

" Set word wrap at 80 characters
set textwidth=80

" Fix our tabs
set tabstop=4
set shiftwidth=4
set expandtab

" Make spelling command
command! Spell :set spell spelllang=en_us

" Configure the status line ---------------------------------------------------
" Always show the status line
set laststatus=2

" Format the status line
set statusline=
" Set paste status
set statusline+=%#MatchParen#
set statusline+=%{&paste?'[paste]':''} " Maybe would be nice if there was regular color between items.
" Add git branch
set statusline+=%#PmenuSel#
"set statusline+=%{exists('g:loaded_fugitive')?fugitive#head():''}
set statusline+=%{exists('g:loaded_fugitive')?FugitiveHead():''}
" Set the current file name
set statusline+=%#CursorColumn#
set statusline+=\ %f
" Set the modified flag (when a file has been modified)
set statusline+=%m
" Set the read only flag ([RO] when we are looking at a read only file)
set statusline+=%r
" Set the help flag ([help] when text is a help file)
set statusline+=%r

" Left align
set statusline+=%=

" Set the syntax checker
"set statusline+=%#error#
"set statusline+=%{SyntasticStatuslineFlag()}
"set statusline+=%*
" Set file type
set statusline+=%#CursorColumn#
set statusline+=%y
" Set the file encoding (utf-8 usually)
set statusline+=\ %{&fileencoding?&fileencoding:&encoding}
" Set the file format (priority: unix, windows, mac)
set statusline+=\[%{&fileformat}\]
" Set the percentage through the file
set statusline+=\ %p%%
" Set the current line and column
set statusline+=\ %l/%L:%c
